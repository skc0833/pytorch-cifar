'''Train CIFAR10 with PyTorch.'''
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torch.backends.cudnn as cudnn

import torchvision
import torchvision.transforms as transforms

import os
import argparse

from models import *
from utils import progress_bar

import timeit
import shutil
from tensorboardX import SummaryWriter
from torchvision.models.resnet import resnet18

parser = argparse.ArgumentParser(description='PyTorch CIFAR10 Training')
parser.add_argument('--lr', default=0.1, type=float, help='learning rate')
parser.add_argument('--resume', '-r', action='store_true',
                    help='resume from checkpoint')
parser.add_argument('--log_dir', type = str, default = 'log', 
                    help = 'The directory to save log.log')
parser.add_argument('--tensorboardx_logdir', type = str, default = 'tensorboardx',
                    help = 'The directory to save tensorboardx logs')
args = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'
best_acc = 0  # best test accuracy
start_epoch = 0  # start from epoch 0 or last checkpoint epoch

EPOCH_CNT = 200
BATCH_SIZE = 512

# Acc: 88.070% torchvision.models.resnet.resnet18, bs512, w4, 200ep, pretrained! -> poor performance
# *Acc: 95.350%	# ResNet18, bs512, w4, 200ep, 32m
# Acc: 94.520%	# ResNet18, bs1024, w4, 200ep
# Acc: 93.230%	# ResNet50, bs1024, w4, 200ep, 2h 5m
# Acc: 92.140%	# ResNet18, bs5120, w4, 200ep

# $ tensorboard --logdir=./log/tensorboardx
# http://8TITANRTX:6006/
if not os.path.exists(args.log_dir):
    os.makedirs(args.log_dir)
tensorboardx_logdir = os.path.join(args.log_dir, args.tensorboardx_logdir)
if os.path.exists(tensorboardx_logdir):
    shutil.rmtree(tensorboardx_logdir)
writer = SummaryWriter(log_dir=tensorboardx_logdir)
args.writer = writer

# Data
print('==> Preparing data..')
transform_train = transforms.Compose([
    transforms.RandomCrop(32, padding=4),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])

transform_test = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])

start = timeit.default_timer()
trainset = torchvision.datasets.CIFAR10(
    root='./data', train=True, download=True, transform=transform_train)
trainloader = torch.utils.data.DataLoader(
    trainset, batch_size=BATCH_SIZE, shuffle=True, num_workers=4) # org batch_size=128, num_workers=2

testset = torchvision.datasets.CIFAR10(
    root='./data', train=False, download=True, transform=transform_test)
testloader = torch.utils.data.DataLoader(
    testset, batch_size=BATCH_SIZE, shuffle=False, num_workers=4) # org batch_size=100, num_workers=2
stop = timeit.default_timer()
print('data loading Time: %.2f s. ' % (stop - start))

classes = ('plane', 'car', 'bird', 'cat', 'deer',
           'dog', 'frog', 'horse', 'ship', 'truck')

# Model
print('==> Building model..')
start = timeit.default_timer()
# net = VGG('VGG19')
# net = ResNet18()
net = resnet18(num_classes=10) # torchvision.models.resnet.resnet18 is better acc, speed than ResNet18
# net = resnet18(pretrained=True, num_classes=1000) # for pretrained model, num_classes should be 1000(#ImageNet class)
# net.fc = nn.Linear(512, 10) # and net.fc should be reset to 10 class
# net = PreActResNet18()
# net = GoogLeNet()
# net = DenseNet121()
# net = ResNeXt29_2x64d()
# net = MobileNet()
# net = MobileNetV2()
# net = DPN92()
# net = ShuffleNetG2()
# net = SENet18()
# net = ShuffleNetV2(1)
# net = EfficientNetB0()
# net = RegNetX_200MF()
# net = SimpleDLA()
net = net.to(device)
if device == 'cuda':
    net = torch.nn.DataParallel(net)
    cudnn.benchmark = True

stop = timeit.default_timer()
print('model building Time: %.2f s. ' % (stop - start))

if args.resume:
    # Load checkpoint.
    print('==> Resuming from checkpoint..')
    assert os.path.isdir('checkpoint'), 'Error: no checkpoint directory found!'
    checkpoint = torch.load('./checkpoint/ckpt.pth')
    net.load_state_dict(checkpoint['net'])
    best_acc = checkpoint['acc']
    start_epoch = checkpoint['epoch']

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=args.lr,
                      momentum=0.9, weight_decay=5e-4)
scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=EPOCH_CNT)


# Training
def train(epoch):
    print('\nEpoch: %d' % epoch)
    net.train()
    train_loss = 0
    correct = 0
    total = 0
    for batch_idx, (inputs, targets) in enumerate(trainloader):
        inputs, targets = inputs.to(device), targets.to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        _, predicted = outputs.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()

        global_batch_idx = epoch * len(trainloader) + batch_idx
        args.writer.add_scalar("Training_Loss", train_loss/(batch_idx+1), global_batch_idx + 1)
        args.writer.add_scalar("Training_Acc", 100.*correct/total, global_batch_idx + 1)

        progress_bar(batch_idx, len(trainloader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
                     % (train_loss/(batch_idx+1), 100.*correct/total, correct, total))


def test(epoch):
    global best_acc
    net.eval()
    test_loss = 0
    correct = 0
    total = 0
    with torch.no_grad():
        for batch_idx, (inputs, targets) in enumerate(testloader):
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = net(inputs)
            loss = criterion(outputs, targets)

            test_loss += loss.item()
            _, predicted = outputs.max(1)
            total += targets.size(0)
            correct += predicted.eq(targets).sum().item()

            global_batch_idx = epoch * len(testloader) + batch_idx
            args.writer.add_scalar("Test_Loss", test_loss/(batch_idx+1), global_batch_idx + 1)
            args.writer.add_scalar("Test_Acc", 100.*correct/total, global_batch_idx + 1)

            progress_bar(batch_idx, len(testloader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
                         % (test_loss/(batch_idx+1), 100.*correct/total, correct, total))

    # Save checkpoint.
    acc = 100.*correct/total
    if acc > best_acc:
        print('Saving..')
        state = {
            'net': net.state_dict(),
            'acc': acc,
            'epoch': epoch,
        }
        if not os.path.isdir('checkpoint'):
            os.mkdir('checkpoint')
        torch.save(state, './checkpoint/ckpt.pth')
        best_acc = acc


for epoch in range(start_epoch, start_epoch+EPOCH_CNT):
    start = timeit.default_timer()
    train(epoch)
    test(epoch)
    scheduler.step()
    stop = timeit.default_timer()
    print('loop Time: %.2f s. ' % (stop - start))
